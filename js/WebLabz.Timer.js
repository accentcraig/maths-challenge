if (typeof WebLabz == 'undefined') WebLabz = {};

WebLabz.Timer = new Class({
    Implements: [Options, Events],

    duration: null,
    startedAt: null,
    stoppedAt: null,

    options: {
        pollInterval: 250, // default poll every quarter second
        onBegin: (function() {}),
        onPoll: (function() {}),
        onStop: (function() {}),
        onTimeUp: (function() {})
    },

    initialize: function (duration, options) {
        this.setOptions(options);
        this.duration = duration;
    },

    setDuration: function (duration) {
        this.duration = duration;
    },
    
    reset: function() {
    	this.startedAt = null;
    	this.endTime = null;
    },

    start: function () {
        this.startedAt = new Date();
        this.stoppedAt = null;
        this.endTime = new Date(this.startedAt.getTime()+(this.duration*1000));
        this.fireEvent('begin');
        this._startPolling();
    },
    
    forceStop: function() { return this.stop(true); },
    
    stop: function (cancelEvent) {
    	var cancelEvent = cancelEvent || false;
        this.stoppedAt = new Date();
        if (!cancelEvent) {
        	this.fireEvent('stop', [this.getTimeRemaining(), this]);
        }
        this._stopPolling();
    },
    
    complete: function() {
    	this.stoppedAt = new Date();
        this.fireEvent('timeUp', [0, this]);
        this._stopPolling();
    },
    
    getTimeElapsed: function() {
    	var now = new Date(), elapsed = now.getTime()-this.startedAt.getTime();
    	return elapsed/1000;
    },
    
    getTimeRemaining: function() {
        var now = new Date(),
        	remaining = this.endTime.getTime()-now.getTime();
        return remaining > 0 ? remaining/1000 : 0;
    },
    
    isRunning: function() { return this.getTimeRemaining() === 0; },
    
    _startPolling: function() {
    	this.timer = setInterval(function() {
    		var remaining = this.getTimeRemaining();
    		
    		this.fireEvent('poll', [remaining, this]);
    		
    		if (remaining == 0) {
    			this.complete();
    		}
    	}.bind(this), this.options.pollInterval);    	
    },
    
    _stopPolling: function() {
    	clearInterval(this.timer);
    }
    
});