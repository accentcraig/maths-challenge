if (typeof WebLabz == 'undefined') WebLabz = {};

WebLabz.Challenge = new Class({
    Implements: [Options, Events],

    possibleValues: [],
    questions: [], // populated at generation
    answers: [], // populated at completion
    correctAnswers: [],
    
    operator: {
        operator: '*',
        display: 'x',
        text: 'multiply'
    },

    isGenerated: false,
    isCompleted: false,
    isGraded: false,

    options: {
        duration: 300, // duration in seconds
        questionCount: 140,
        minValue: 0,
        maxValue: 12,
        
        answerElType: 'li',
        timerEl: null,
        
        thresholds: [],
        
        onGenerate: (function() {}),
        onStart:  (function() {}),
        onTimeUpdate: (function() {}),
        onTimeUp:  (function() {}),
        onGrade: (function() {})
    },

    initialize: function (el, options) {
        this.setOptions(options);
        this.challengeArea = $(el);
        this._populatePossibleValues();
        this.timer = new WebLabz.Timer(this.options.duration, {
        	onPoll: function(remaining) { this._updateTimer(remaining) }.bind(this),
        	onStop: function(remaining) { this.complete(); }.bind(this),
        	onTimeUp: function(remaining) { this.timeUp(); }.bind(this)
        });
        return this;
    },

    generate: function () {
    	// clear
        this.challengeArea.empty();

        // create new set of questiosn
        this._populateQuestions();

        // display questions
        this.questions.each(function (el, i) {
            new Element('li').set('html', this._createQuestionHtml(i)).inject(this.challengeArea);
        }.bind(this));
        
        this.isGenerated = true;  
        this._preventAnswering(); // .. until started
        this.fireEvent('generate');
    },

    start: function () {
    	this.timer.start();
    	this._allowAnswering();
    	this.fireEvent('start');
    },
    
    timeUp: function() {
    	this.fireEvent('timeUp');
    	this.complete();
    },
    
    complete: function() {
    	this.isCompleted = true;
    	this._preventAnswering();
    	this._populateAnswers();
    	this.fireEvent('complete');
    },
    
    grade: function() {
    	var inputs = this.challengeArea.getElements('input.answer'),
    		answers = this.answers;
    	
    	if (this.isGraded) return;
    	
    	if (!this.isCompleted) {
    		this.timer.stop();
    		answers = this.answers;
    	}
    	
    	answers.each(function(answer, i) {
        	if (this._answerIsCorrect(i)) {
        		this.correctAnswers.push(i);
        		inputs[i].addClass('correct');
        	}
        }.bind(this));
    	
    	this.isGraded = true;
    	this.fireEvent('grade', this.getScore());
    },
    
    getScore: function() {
    	return this.correctAnswers.length;
    },

    // PRIVATE
    
    _getCorrectAnswer: function(i) {
    	var correct;
    	eval('correct = ' + this.questions[i][0] + this.operator.operator + this.questions[i][1]);
    	return correct;
    },
    
    _answerIsCorrect: function(i) {
    	var correct = this._getCorrectAnswer(i), provided = this.answers[i].replace(/[^0-9]*/g, '');
    	return provided != '' && correct == provided;
    },
    
    _allowAnswering: function() {
    	this.challengeArea.getElements('input.answer').set('disabled', false);
    },
    
    _preventAnswering: function() {
    	this.challengeArea.getElements('input.answer').set('disabled', true);
    },

    _updateTimer: function(remaining) {
    	if ($(this.options.timerEl)) {
    		$(this.options.timerEl).set('html', remaining);
    	}
    	this.fireEvent('timeUpdate', remaining);
    },
    
    _populatePossibleValues: function () {
        var possible = [];
        for (var i = this.options.minValue; i <= this.options.maxValue; i++) {
            possible.append([i]);
        }
        this.possibleValues = possible;
    },

    _populateQuestions: function () {
        var questions = [];
        while (questions.length < this.options.questionCount) {
            questions.append([this._generateSumValues()]);
        }
        this.questions = questions;
    },
    
    _populateAnswers: function () {
        var answers = [];
        this.challengeArea.getElements('input.answer').each(function(el, i) {
        	answers[i] = el.get('value');
        });
        this.answers = answers;
    },

    _generateSumValues: function () {
        return [this.possibleValues.getRandom(), this.possibleValues.getRandom()];
    },

    _createQuestionHtml: function (i) {
        var question = this.questions[i];
        return '{first} {operator} {second} {equals} <input type="number" class="answer" />'.substitute({
            first: question[0],
            second: question[1],
            operator: this.operator.display,
            equals: '='
        });
    }

});