window.addEvent('domready', function () {
	var countdown = 5;
	
	$('actionBar').fade('hide');
	$('result').setStyle('display', 'none');
    
    $$('.challengeStart').addEvent('click', function (e) {
    	$('challengeArea').empty().adopt(new Element('ul', {id:'challengeQuestions'}));
    	challenge = new WebLabz.Challenge($('challengeQuestions'), {
        	questionCount: $('challengeQuestionCount').get('value'),
        	duration: $$('#challengeQuestionCount option:selected').get('data-duration')[0].toInt(),
        	onStart: function() {
        		$('challengeSetup').addClass('hide');
        		$('actionBar').fade('in');
        	},
        	onTimeUpdate: function(remaining) {
        		var m = (remaining / 60).toInt(), s = remaining.toInt() % 60;
        		$$('.timer span.count').set('html', '' + m + ':' + s + '');
         	},
        	onTimeUp: function() {
        		alert('Your time is up! Please press the "I\'ve finished" button to see how you\'ve done!')
        	}
        });
    	
        challenge.generate();
        challenge.start();
        e.stop();
    });
    
    $$('.challengeGrade').addEvent('click', function (e) {
    	$(this).set('disabled', true);
    	$('actionBar').fade('out');
    	
        challenge.grade();
        
        $('result').getElement('.grade').set('text', challenge.getScore());
        $('result').getElement('.totalQuestions').set('text', challenge.questions.length);
        $('result').setStyle('display', '');
        
        e.stop();
    });
});